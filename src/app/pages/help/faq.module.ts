import { FaqComponent } from 'app/pages/help/faq.component';

import { NgModule } from '@angular/core';
import { MatExpansionModule, MatIconModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
  {
    path: "help",
    component: FaqComponent
  }
];

@NgModule({
  declarations: [FaqComponent],
  imports: [
    RouterModule.forChild(routes),

    MatExpansionModule,
    MatIconModule,

    FuseSharedModule
  ]
})
export class FaqModule {}
